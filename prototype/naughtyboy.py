import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk
import threading
import logging
import os
import cairo
import irc.client

logger = logging.getLogger(__name__)
VERSION = "0.1-a"

CHANNEL = '#sebdevsdk48jf3'
SERVER = 'irc.freenode.net'
PORT = 6667
NICK = 'naughtyboy_cl'

COLOUR = 0, 0, 0, .5
INFO_COLOUR = .3, .1, .2, .5

class Irc(irc.client.SimpleIRCClient):
    def __init__(self, window):
        irc.client.SimpleIRCClient.__init__(self)
        self.window = window

    def on_welcome(self, connection, event):
        logger.info('Connection complete, Joining channel')
        GLib.idle_add(self.window.__show_info_wait, 'Connection complete, Joining channel {0} ...'.format(CHANNEL))
        connection.join(CHANNEL)

    def on_join(self, c, e):
        logger.info("Joined {0}".format(e.target))
        GLib.idle_add(self.window.joined_channel, e.target)

    def on_pubmsg(self, c, e):
        logger.info("ChanMSG: {0} {1} {2} {3}".format(e.arguments, e.source, e.target, e.type))
        GLib.idle_add(self.window.update_channel, e.arguments[0], e.source.split('!')[0])

    def say_on_channel(self, message):
        self.connection.privmsg(CHANNEL, message)


class IrcWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title='NaughtyBoy IRC Client (v{0})'.format(VERSION))
        self.set_default_size(800, 600)

        # Check for compositing support
        self.screen = self.get_screen()
        self.visual = self.screen.get_rgba_visual()
        if self.visual != None and self.screen.is_composited():
            logger.info("Compositing support detected!")
            self.set_visual(self.visual)

        #
        box = Gtk.VBox(spacing=2)
        self.add(box)


        # Add info bar at top
        self.info_bar = Gtk.InfoBar()
        self.info_bar.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(*INFO_COLOUR))
        self.spinner = Gtk.Spinner()
        self.wait_label = Gtk.Label("Connecting to {0} ...".format(SERVER))
        self.info_bar.get_content_area().pack_start(self.wait_label, True, True, 5)
        self.info_bar.get_action_area().pack_start(self.spinner, True, True, 5)
        box.pack_start(self.info_bar, False, False, 0)

        # Add channel text view to box
        self.channel_view = Gtk.TextView()
        self.channel_view.set_editable(False)
        self.channel_view.set_cursor_visible(False)
        self.channel_view.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(*COLOUR))
        box.pack_start(self.channel_view, True, True, 0)

        # Add Text input view to box
        self.entry = Gtk.Entry()
        self.entry.set_text('SPEAK!')
        self.entry.connect("activate", self.on_message_entered)
        self.entry.set_opacity(0.5)
        self.entry.set_editable(False)
        box.pack_end(self.entry, False, False, 0)

        # Set up IRC Connection (will probably hang UI, should be on own thread)
        self.client = Irc(self)
        logger.info("Connecting to irc server {0}:{1} with nick {2}".format(SERVER, PORT, NICK))
        try:
            self.client.connect(SERVER, PORT, NICK)
        except irc.client.ServerConnectionError as x:
            self.channel_view.get_buffer.set_text("Failed connecting: {0}".format(x.message))

        self.spinner.start()
        self.info_bar.show()

        self.irc_thread = threading.Thread(target=self.client.start)
        self.irc_thread.setDaemon(True)
        self.irc_thread.start()

        self.set_app_paintable(True)
        self.connect("draw", self.area_draw)

        self.connect("delete-event", self.close_and_quit)

    def joined_channel(self, channel_name):
        """
        Callback used by the irc client when joined
        :param channel_name: The name of the channel that has just been joined
        """
        self.hide_info()
        self.channel_view.get_buffer().set_text("** JOINED {0} **".format(channel_name))
        self.entry.set_opacity(1)
        self.entry.set_editable(True)

    def on_message_entered(self, entry):
        self.client.say_on_channel(entry.get_text())
        self.update_channel('\t"{0}"'.format(entry.get_text()), 'You')
        self.entry.set_text("")

    def replace_text_view(self, text):
        self.channel_view.get_buffer().set_text(text)

    def update_channel(self, message, who):
        self.channel_view.get_buffer().insert_at_cursor('\n')
        self.channel_view.get_buffer().insert_at_cursor('{0}: {1}'.format(who, message))

    def close_and_quit(self, window, event):
        Gtk.main_quit()
        logger.info("Quitting!")
        print("Terminating connections...")
        self.client.connection.disconnect(message='Byeeee!')


    def show_info_wait(self, wait_message):
        self.wait_label.set_text(wait_message)
        self.spinner.start()
        self.info_bar.show()

    def hide_info(self):
        self.spinner.stop()
        self.info_bar.hide()

    def area_draw(self, widget, cr):
        cr.set_source_rgba(*COLOUR)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.paint()
        cr.set_operator(cairo.OPERATOR_OVER)

if __name__ == "__main__":
    user_home = os.path.expanduser('~')
    config_dir = os.path.join(user_home, '.naughtyboy')
    if not os.path.exists(config_dir):
        os.mkdir(config_dir)

    logging.basicConfig(filename=os.path.join(config_dir, 'debug.log'), level=logging.INFO)
    logger.info("Starting up...")
    win = IrcWindow()
    win.show_all()
    Gtk.main()