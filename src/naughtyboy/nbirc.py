"""
    NaughtyBoy IRC Client, uses the python-irc library
"""

import irc.client
import threading
import logging
import os
import cairo
import pickle
import datetime
import sys
import gi
import random


gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk, Pango, Gio
from math import sin
from time import sleep

logger = logging.getLogger(__name__)
VERSION = 0.1

OPACITY_SETTINGS = {
    'general': 0.8,
    'irc': 0.7,
    'entry': 0.9,
    'OVERRIDE': 0,
}

STYLE_CSS = """
    #IrcWindow {
        engine: none;
    }

    #IrcWindow GtkEntry {
        color: #FFF;
    }

    #IrcWindow GtkTextView {
        color: #BBB;
        font-family: monospace;

    }

    #IrcWindow GtkButton {
        background-color: #331111;
        color: #FFF;
    }
"""

SERVER_DETAILS_FILE = 'servers.p'
UI_PREFS_FILE = 'uiprefs.p'
CONFIG_DIR = '.naughtyboy'

PREF_INFO_BG = 'info_bgcol'
PREF_IRC_BG = 'irc_bgcol'
PREF_ENTRY_BG = 'entry_bgcol'
PREF_SERVER = 'server'
PREF_NICK = 'nick'
PREF_PORT = 'port'
PREF_CHAN = 'channel'
PREF_CHANNELS = 'channels'

DEFAULT_USER_PREFS = {
    PREF_IRC_BG: (0, 0, 0, OPACITY_SETTINGS['irc']),
    PREF_ENTRY_BG: (0, 0, 0, 1),
    PREF_INFO_BG: (.3, .1, .2, OPACITY_SETTINGS['general']),
    'time_fmt': '%H:%M',
}

DEFAULT_SERVER_DETAILS = {
    PREF_NICK: 'nbirc',
    PREF_PORT: 6667,
    PREF_SERVER: 'irc.freenode.net',
    PREF_CHAN: '#sebdevsdf78yh8f',
    PREF_CHANNELS: ['#swbdev', '#swbtest']
}

MAX_MSG_LEN = 300


class GetServerDetailsDialog(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Enter server details...", parent, 0,
                            (Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_default_size(1, 180)

        label = Gtk.Label("Enter yo deets!")
        self.server_entry = Gtk.Entry()
        self.nick_entry = Gtk.Entry()
        self.port_entry = Gtk.Entry()
        self.channel_entry = Gtk.Entry()
        self.server_entry.set_placeholder_text('server address')
        self.nick_entry.set_placeholder_text('nickname')
        self.port_entry.set_placeholder_text('port number')
        self.channel_entry.set_placeholder_text('default channel')

        box = self.get_content_area()
        box.pack_start(label, True, True, 0)
        box.pack_start(self.server_entry, False, False, 0)
        box.pack_start(self.port_entry, False, False, 0)
        box.pack_start(self.nick_entry, False, False, 0)
        box.pack_start(self.channel_entry, False, False, 0)

        self.show_all()

    def get_server_details(self):
        nick = self.nick_entry.get_text()
        port = self.port_entry.get_text()
        server = self.server_entry.get_text()
        channel = self.channel_entry.get_text()

        try:
            port = int(port)
        except ValueError:
            return None

        if not (len(nick) and len(server) and channel.startswith('#')):
            return None

        return {PREF_SERVER: server, PREF_CHAN: channel, PREF_PORT: port, PREF_NICK: nick}


class IrcController(irc.client.SimpleIRCClient):
    def __init__(self, view, server_details):
        irc.client.SimpleIRCClient.__init__(self)
        self.view = view
        self.nick = server_details[PREF_NICK]
        self.server = server_details[PREF_SERVER]
        self.port = server_details[PREF_PORT]
        self.default_channel = server_details[PREF_CHAN]
        self.joined_channels = []
        self.topics = {}
        self.nicks = {}
        self.ops = {}

        if PREF_CHANNELS in server_details.keys():
            self.autojoin = server_details[PREF_CHANNELS]
        else:
            self.autojoin = None

    def say_to(self, channel, message):
        messages = [x for x in message.split('\n') if x]
        for m in messages:
            m = unicode(m, 'utf-8')
            logger.info("Saying {0} to {1}".format(m, channel))
            try:
                self.connection.privmsg(channel, m)
            except irc.client.MessageTooLong:
                logger.info("Truncating long message")
                subs = truncate(m, MAX_MSG_LEN)
                for sm in subs:
                    logger.info("Sending part: {0}".format(sm))
                    self.connection.privmsg(channel, sm)
        return messages

    def is_operator(self, nick, channel):
        if channel in self.ops.keys():
            return nick in self.ops[channel]
        return False

    def list_names_for_channel(self, channel):
        logger.info("Request name list for {0}".format(channel))
        self.connection.names([channel])

    def connect_to_server(self):
        self.connect(self.server, self.port, self.nick)

    def part_channels(self, channels, message=None):
        self.connection.part(channels, message)

    def on_welcome(self, connection, event):
        logger.info('Connection complete, Joining channel')
        if self.autojoin is not None:
            GLib.idle_add(self.view.show_info_wait, 'Connection complete, Joining channels {0} ...'.format(self.autojoin))
            for chan in self.autojoin:
                connection.join(chan)
        else:
            GLib.idle_add(self.view.show_info_wait, 'Connection complete, Joining channel {0} ...'.format(self.default_channel))
            connection.join(self.default_channel)

    def on_join(self, c, e):
        logger.info('JOIN: {0} {1} {2} {3}'.format(e.arguments, e.source, e.target, e.type))
        if get_nick(e.source) == self.nick:
            # We are joining the channel
            self.joined_channels.append(e.target)
            self.nicks[e.target] = []
            GLib.idle_add(self.view.joined_new_channel, e.target)
        else:
            # Someone else is joining
            self.nicks[e.target].append(get_nick(e.source))
            GLib.idle_add(self.view.user_joining_channel, e.source, e.target)

    def on_currenttopic(self, c, e):
        logger.info('CURRENT TOPIC: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))
        channel, topic = e.arguments
        self.topics[channel] = topic
        GLib.idle_add(self.view.set_channel_topic, topic, channel)

    def on_topicinfo(self, c, e):
        logger.info('TOPIC INFO: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_topic(self, c, e):
        logger.info('TOPIC: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))
        channel, topic = e.target, e.arguments[0]
        nick = get_nick(e.source)
        self.topics[channel] = topic
        GLib.idle_add(self.view.update_topic, topic, channel, nick)

    def on_pubmsg(self, c, e):
        logger.info("ChanMSG: {0} {1} {2} {3}".format(e.arguments, e.source, e.target, e.type))
        GLib.idle_add(self.view.append_channel_message, e.arguments[0].encode('utf-8'), e.source.split('!')[0], e.target)

    def on_part(self, c, e):
        logger.info('PART: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))
        user, channel = e.source, e.target
        nick = get_nick(user)

        if channel in self.joined_channels and nick == self.nick:
            self.joined_channels.remove(channel)
            GLib.idle_add(self.view.leaving_channel, channel)
        else:
            if nick in self.nicks[channel]:
                self.nicks[channel].remove(nick)

            GLib.idle_add(self.view.user_leaving_channel, user, channel)

    def on_error(self, c, e):
        logger.info('IRC_ERROR: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_kick(self, c, e):
        logger.info('KICK: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_mode(self, c, e):
        logger.info('MODE: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_ping(self, c, e):
        logger.info('PING: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_privmsg(self, c, e):
        logger.info('PRIVMSG: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))
        GLib.idle_add(self.view.received_privmsg, get_nick(e.source), e.arguments[0])

    def on_privnotice(self, c, e):
        logger.info('PRIVNOTICE: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_pubnotice(self, c, e):
        logger.info('PUBNOTICE: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_quit(self, c, e):
        logger.info('IRC_QUIT: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))
        GLib.idle_add(self.view.user_quitting, e.source, e.arguments[0].encode('utf-8'))

    def on_invite(self, c, e):
        logger.info('INVITE: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_pong(self, c, e):
        logger.info('PONG: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_action(self, c, e):
        logger.info('ACTION: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_nick(self, c, e):
        logger.info('NICK: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))

    def on_namreply(self, c, e):
        logger.info('Names: {0} || {1} || {2} || {3}'.format(e.arguments, e.source, e.target, e.type))
        op_token, channel, names = e.arguments
        names = names.strip()
        nicknames = [name.lstrip('@') for name in names.split(' ')]
        self.ops[channel] = [nick for nick in names.split() if nick.startswith('@')]
        self.nicks[channel].extend(nicknames)
        GLib.idle_add(self.view.nicks_in_channel, nicknames, channel)


class IrcView(Gtk.Window):
    def __init__(self, config_dir):
        Gtk.Window.__init__(self, title='NaughtyBoy IRC Client (v{0})'.format(VERSION))

        #self.set_decorated(False)
        self.set_default_size(1024, 768)
        self.current_completions = None
        self.channel_buffers = {}
        self.channel_buttons = {}
        self.nick_buttons = {}
        self.active_window = None
        self.input_history = []
        self.next_history = None
        self.nick_colours = {}
        self.colour_spacing = 3
        self.colour_offset = random.randint(0, 30)

        self.hb = Gtk.HeaderBar()
        self.hb.set_show_close_button(True)
        self.hb.props.title = "nbirc v{0}".format(VERSION)
        self.set_titlebar(self.hb)

        logger.info("Checking for compositing support")
        self.screen = self.get_screen()
        self.visual = self.screen.get_rgba_visual()
        if self.visual != None and self.screen.is_composited():
            logger.info("Compositing support detected!")
            self.set_visual(self.visual)
        else:
            logger.warning("No compositing :(")

        saved_servers_file = os.path.join(config_dir, SERVER_DETAILS_FILE)
        ui_prefs_file = os.path.join(config_dir, UI_PREFS_FILE)

        self.saved_servers_file = saved_servers_file

        if os.path.exists(ui_prefs_file):
            logger.info("Found ui prefs file")
            ui_prefs = pickle.load(open(ui_prefs_file, 'rb'))
        else:
            logger.info("No ui prefs file, creating with defaults")
            ui_prefs = DEFAULT_USER_PREFS
            pickle.dump(ui_prefs, open(ui_prefs_file, 'wb'))

        if os.path.exists(saved_servers_file):
            logger.info("Found saved servers")
            server_details = pickle.load(open(saved_servers_file, 'rb'))
        else:
            logger.info("Querying user for server details")
            server_details = None

        if server_details is None:
            server_details = self.__get_server_details_with_dialog()
            logger.info("Saving credentials {0} in {1}".format(server_details, saved_servers_file))
            pickle.dump(server_details, open(saved_servers_file, 'wb'))

        self.ui_prefs = ui_prefs
        self.__setup_window(ui_prefs)
        self.ic = IrcController(self, server_details)
        self.server_details = server_details

        logger.info("Connecting to irc server {0}:{1} with nick {2}".format(self.ic.server, self.ic.port, self.ic.nick))
        try:
            self.ic.connect_to_server()
        except irc.client.ServerConnectionError as x:
            self.channel_view.get_buffer.set_text("Failed connecting: {0}".format(x.message))
            return

        self.show_info_wait("Connecting to {0} ...".format(self.ic.server))
        self.irc_thread = threading.Thread(target=self.ic.start)
        self.irc_thread.setDaemon(True)
        self.irc_thread.start()

        self.connect("delete-event", self.on_close_and_quit)
        # Override area draw for proper between-widget transparency
        self.set_app_paintable(True)
        self.connect("draw", self.on_area_draw)
        self.show_all()

    def __setup_window(self, ui_prefs):
        """ Set up the UI window

        :param ui_prefs: Dict of user preferences
        """
        container_box = Gtk.HBox(spacing=0)


        box = Gtk.VBox(spacing=0)
        self.add(box)

        self.set_name('IrcWindow')
        numix = Gtk.CssProvider()
        numix.load_from_file(Gio.File.new_for_path(get_resource_path('../../res/style/gtk.css')))

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            numix,
            Gtk.STYLE_PROVIDER_PRIORITY_SETTINGS
        )

        custom_style = Gtk.CssProvider()
        custom_style.load_from_data(STYLE_CSS)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            custom_style,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        self.set_icon_from_file(get_resource_path('../../res/icon.png'))

        # Add info bar at top
        self.info_bar = Gtk.InfoBar()
        self.info_bar.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(*ui_prefs[PREF_INFO_BG]))
        self.spinner = Gtk.Spinner()
        self.wait_label = Gtk.Label()
        self.info_bar.get_content_area().pack_start(self.wait_label, True, True, 0)
        self.info_bar.get_action_area().pack_start(self.spinner, True, True, 0)
        box.pack_start(self.info_bar, False, False, 0)

        # Add channel text view to box
        self.scrolled_view = Gtk.ScrolledWindow.new()
        self.channel_view = Gtk.TextView()
        self.channel_view.set_editable(False)
        self.channel_view.set_cursor_visible(False)

        self.channel_view.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        self.channel_view.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(*ui_prefs[PREF_IRC_BG]))
        self.channel_view.set_indent(-42)
        self.channel_view.set_pixels_inside_wrap(-1)
        self.channel_view.set_pixels_below_lines(0)
        self.tag_bold = self.channel_view.get_buffer().create_tag("bold", weight=Pango.Weight.BOLD, foreground='white')
        self.tag_underline = self.channel_view.get_buffer().create_tag("ul", underline=Pango.Underline.SINGLE)
        self.tag_yellow = self.channel_view.get_buffer().create_tag("yellow",  foreground='yellow')
        self.scrolled_view.add(self.channel_view)
        box.pack_start(self.scrolled_view, True, True, 0)

        self.channel_button_box = Gtk.Box()
        box.pack_end(self.channel_button_box, False, False, 0)

        # Add Text input view to box
        self.entry = Gtk.Entry()
        self.entry.set_placeholder_text('Do speak up dear...')
        self.entry.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.1,0.1,0.1,OPACITY_SETTINGS['entry']))
        self.entry.set_editable(False)
        self.entry.set_can_focus(False)
        self.entry.connect("activate", self.on_message_entered)
        self.entry.connect('key-press-event', self.on_keypress)
        box.pack_end(self.entry, False, False, 0)


    def __format_message(self, who, message, channel):
        """ Main message formatting method

        :param who: Who sent the message
        :param message: What the message was
        :param channel: Where was it sent (if None, assumes privmsg)
        :return: The formatted message as a string
        """
        timestamp = datetime.datetime.now().strftime(self.ui_prefs['time_fmt'])
        line = '{2} <{0}> {1}'.format(who, message, timestamp)
        return line

    def __replace_current_input_word(self, replacement):
        words = self.entry.get_buffer().get_text().split(' ')
        words[-1] = replacement
        self.entry.set_text(' '.join(words))
        self.entry.set_position(self.entry.get_buffer().get_length())

    def __replace_current_input(self, replacement):
        self.entry.set_text(replacement)
        self.entry.set_position(self.entry.get_buffer().get_length())

    def __get_server_details_with_dialog(self):
        dialog = GetServerDetailsDialog(self)
        dialog.set_modal(True)
        server_details = None

        while server_details is None:
            dialog.run()
            server_details = dialog.get_server_details()

        dialog.destroy()
        return server_details

    def __get_nick_colour(self, nick):
        coloured_nicks = self.nick_colours.keys()
        if nick not in coloured_nicks:
            self.nick_colours[nick] = get_palette_colour(self.colour_offset + len(coloured_nicks)*self.colour_spacing)

        return self.nick_colours[nick]



    def __new_privmsg_window(self, who, first_message=None):
        nick_button = self.__create_nick_button(who)
        self.channel_button_box.pack_end(nick_button, False, False, 0)
        self.nick_buttons[who] = nick_button
        buffer = self.__setup_text_buffer(who)
        buffer.create_tag(who, foreground_rgba=self.__get_nick_colour(who))
        buffer.create_tag(self.ic.nick, foreground_rgba=self.__get_nick_colour(self.ic.nick))
        logger.info('Creating priv tag for {0}, {1}'.format(who, self.ic.nick))
        self.channel_view.set_buffer(buffer)
        self.channel_buffers[who] = buffer
        self.channel_button_box.show_all()
        self.hb.set_subtitle('Discourse with {0}'.format(who))
        self.active_window = who
        self.__set_button_opacities()
        if first_message:
            self.append_channel_message(first_message, who, who)

    def __switch_windows(self, direction=1):
        all_windows = [w.get_name() for w in self.channel_button_box.get_children()]
        logger.info("Switching between {0}: current is {1}".format(all_windows, self.active_window))
        active_index = all_windows.index(self.active_window)
        next_index = (active_index + direction) % len(all_windows)
        switch_to_button = self.channel_button_box.get_children()[next_index]
        switch_to_button.clicked()

    def __process_command(self, command_with_args):
        command, args = command_with_args[0], command_with_args[1:]
        logger.info("Processing command {0} with args {1}".format(command, args))

        if command == 'join' or command == 'j':
            self.ic.connection.join(args[0])
            return True

        if command == 'part' or command == 'p':
            if self.active_window in self.ic.joined_channels:
                msg = ' '.join(args) if args else None
                self.ic.part_channels([self.active_window], msg)
                self.__destroy_channel_button(self.active_window)
                return True

        if command == 'msg' or command == 'pm':
            if len(args) >= 2:
                recipient, text = args[0], ' '.join(args[1:])
                self.ic.connection.privmsg(recipient, text)
                self.__new_privmsg_window(recipient, text)

        if command == 'query':
            if args:
                self.__new_privmsg_window(args[0])


    def __tab_complete_nicks(self, to_complete, nick_list):
        if not nick_list:
            return None

        last = to_complete.split()[-1]

        if last in nick_list:
            return nick_list

        return [nick for nick in nick_list if nick.startswith(last)]

    def __create_channel_button(self, channel):
        channel_button = Gtk.Button(channel)
        channel_button.set_name(channel)
        channel_button.connect("clicked", self.on_channel_button_click)
        channel_button.set_opacity(OPACITY_SETTINGS['general'])
        self.channel_button_box.pack_start(channel_button, False, False, 0)
        return channel_button

    def __destroy_channel_button(self, channel):
        button = self.channel_buttons[channel]
        button.disconnect_by_func(self.on_channel_button_click)
        self.channel_button_box.remove(button)
        button.destroy()

    def __create_nick_button(self, nick):
        button = Gtk.Button(nick)
        button.set_name(nick)
        button.connect('clicked', self.on_nick_button_click)
        button.connect('button-release-event', self.on_right_click_nick_button)
        nc = self.__get_nick_colour(nick)
        button_bg = Gdk.RGBA(nc.red*0.2, nc.green*0.2, nc.blue*0.2, 0.9)
        button.override_background_color(Gtk.StateFlags.NORMAL, button_bg)
        button.set_opacity(OPACITY_SETTINGS['general'])
        return button

    def __destroy_nick_button(self, nick):
        button = self.nick_buttons[nick]
        button.disconnect_by_func(self.on_nick_button_click)
        button.disconnect_by_func(self.on_right_click_nick_button)
        self.channel_button_box.remove(button)
        button.destroy()

    def __setup_text_buffer(self,channel):
        buffer = Gtk.TextBuffer()
        buffer.create_tag("bold", weight=Pango.Weight.BOLD, foreground='white')
        buffer.create_tag("ul", underline=Pango.Underline.SINGLE)
        buffer.create_tag("yellow",  foreground='yellow')
        buffer.create_tag("italic", style=Pango.Style.ITALIC)
        buffer.create_tag("join", style=Pango.Style.ITALIC, foreground_rgba=Gdk.RGBA(0.25, 0.5, 0.25, 1))
        buffer.create_tag("quit", style=Pango.Style.ITALIC, foreground_rgba=Gdk.RGBA(0.5, 0.25, 0.25, 1))

        return buffer

    def __set_button_opacities(self):
        all_buttons = self.channel_buttons.values() +  self.nick_buttons.values()
        for button in all_buttons:
            if button.get_name() == self.active_window:
                button.set_opacity(1)
            else:
                button.set_opacity(OPACITY_SETTINGS['general'])

        if len(self.channel_button_box.get_children()) == 1:
            self.channel_button_box.hide()
        else:
            self.channel_button_box.show_all()

    def __pulse(self, channel):
        if channel not in self.channel_buttons.keys():
            return False

        logger.info("Pulsing {0}".format(channel))
        pulse_time = 1000 # miliseconds
        target_fps = 50
        sleep_time = pulse_time / target_fps
        num_iters = pulse_time / sleep_time

        start_alpha = 0.5
        end_alpha = OPACITY_SETTINGS['general']
        alpha_delta = 0.02

        button = self.channel_buttons[channel]
        alpha = end_alpha
        #assert alpha > start_alpha
        while alpha > start_alpha:
            alpha -= alpha_delta
            GLib.idle_add(button.set_opacity, alpha)
            sleep(0.01)
        #assert alpha < end_alpha
        while alpha < end_alpha:
            alpha += alpha_delta
            GLib.idle_add(button.set_opacity, alpha)
            sleep(0.02)






    def on_area_draw(self, w, cr):
        """ Private callback for overriding drawing
        """
        cr.set_source_rgba(0, 0, 0, OPACITY_SETTINGS['OVERRIDE'])
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.paint()
        cr.set_operator(cairo.OPERATOR_OVER)

    def on_close_and_quit(self, w, e):
        """ Private callback for the clicking on the close 'x'
        """
        current_channels = [b.get_name() for b in self.channel_button_box.get_children() if b.get_name().startswith('#')]
        sd = self.server_details
        sd[PREF_CHANNELS] = current_channels
        pickle.dump(sd, open(self.saved_servers_file, 'wb'))
        print("Terminating connections...")
        self.ic.connection.quit("Byeee!")
        logger.info("Quitting")
        Gtk.main_quit()

    def on_keypress(self, widget, ev, data=None):
        if ev.keyval == Gdk.KEY_Tab or ev.keyval == Gdk.KEY_ISO_Left_Tab:
            if ev.state & Gdk.ModifierType.SHIFT_MASK:
                self.__switch_windows(-1)
                return True
            if ev.state & Gdk.ModifierType.CONTROL_MASK:
                self.__switch_windows()
                return True
            logger.info("Doing Tab Complete")
            input = self.entry.get_text()
            if input.startswith('/'):
                cmd = True
                if ' ' not in input:
                    # cannot tab complete command yet
                    return True
            else:
                cmd = False

            if self.current_completions:
                logger.info("Switching completions")
                self.next_completion = (self.next_completion + 1) % len(self.current_completions)
            else:
                completions = self.__tab_complete_nicks(input, self.ic.nicks[self.active_window])
                logger.info("Found {0}".format(completions))
                if completions:
                    self.current_completions = completions
                    self.next_completion = 0
                else:
                    return True

            completion = self.current_completions[self.next_completion]
            if len(self.current_completions) == 1 and not cmd:
                completion += ', '
            self.__replace_current_input_word(completion)
            return True

        if ev.keyval == Gdk.KEY_BackSpace:
            self.current_completions = None
            return False

        if ev.keyval == Gdk.KEY_Up:
            logger.info('Up pressed')
            if self.next_history is not None:
                self.next_history += 1
            else:
                self.next_history = 1

            try:
                self.__replace_current_input(self.input_history[-self.next_history])
            except IndexError as e:
                self.next_history -= 1
                logger.info('Got index error: {0}'.format(e.message))

            return True

        if ev.keyval == Gdk.KEY_Down:
            logger.info('Down pressed')
            if self.next_history is not None and self.next_history > 1:
                self.next_history -=1
                self.__replace_current_input(self.input_history[-self.next_history])
            else:
                self.entry.set_text('')
                self.next_history = None
            return True

    def on_channel_button_click(self, widget):
        channel = widget.get_name()
        logger.info('Channel button clicked: {0}'.format(channel))
        end = self.channel_buffers[channel].get_insert()
        self.channel_view.set_buffer(self.channel_buffers[channel])
        self.channel_view.move_mark_onscreen(end)
        if channel in self.ic.topics.keys():
            self.hb.set_subtitle(self.ic.topics[channel])
        else:
            self.hb.set_subtitle("")
        self.active_window = channel
        self.__set_button_opacities()
        self.entry.grab_focus()


    def on_nick_button_click(self, widget):
        nick = widget.get_name()
        logger.info('Nick button clicked: {0}'.format(nick))
        self.channel_view.set_buffer(self.channel_buffers[nick])
        self.channel_view.move_mark_onscreen(self.end_markers[nick])
        self.hb.set_subtitle('Discourse with {0}'.format(nick))
        self.active_window = nick
        self.__set_button_opacities()
        self.entry.grab_focus()

    def on_right_click_nick_button(self, widget, event):
        logger.info("Right click nick {0}".format(widget.get_name()))
        if event.button == 3:
            self.__destroy_nick_button(widget.get_name())
        self.__set_button_opacities()

    def on_message_entered(self, entry):
        """ UI callback for text entry signal

        :param entry: The entry widget where text was entered
        """
        self.current_completions = None
        self.next_history = None
        text = entry.get_text()

        if not self.input_history:
            self.input_history.append(text)
        elif text != self.input_history[-1]:
            self.input_history.append(text)


        if text.startswith('/'):
            self.__process_command(text[1:].split(' '))
        else:
            sent_text = self.ic.say_to(self.active_window, text)
            for message in sent_text:
                self.append_channel_message(message, self.ic.nick, self.active_window)

        self.entry.set_text("")


    def show_info_wait(self, wait_message):
        self.wait_label.set_text(wait_message)
        self.spinner.start()
        self.info_bar.show()

    def hide_info_wait(self):
        self.spinner.stop()
        self.info_bar.hide()



    def joined_new_channel(self, channel):
        """ Controller callback for us joining a channel

        :param channel_name: The name of the channel that has just been joined
        """
        self.hide_info_wait()
        self.entry.set_editable(True)
        self.entry.set_can_focus(True)
        self.entry.grab_focus()

        if channel in self.channel_buffers.keys():
            self.append_status('Rejoining {0}'.format(channel), channel)
            self.channel_view.set_buffer(self.channel_buffers[channel])
            self.channel_buttons[channel] = self.__create_channel_button(channel)
            self.channel_button_box.show_all()
            self.active_window = channel
            self.__set_button_opacities()
            return

        channel_button = self.__create_channel_button(channel)

        if len(self.channel_buttons.keys()) > 0:
            self.channel_button_box.show_all()

        self.channel_buttons[channel] = channel_button
        self.channel_buffers[channel] = self.__setup_text_buffer(channel)
        self.channel_buffers[channel].set_text("Joining {0} ...\n".format(channel))
        self.channel_view.set_buffer(self.channel_buffers[channel])

        self.active_window = channel
        self.__set_button_opacities()

    def set_channel_topic(self, topic, channel):
        self.hb.set_subtitle('{0}: {1}'.format(channel, topic))
        self.append_status('Topic for {1}: {0}'.format(topic, channel), channel, 'bold')

    def update_topic(self, topic, channel, who):
        """ Controller callback for when someone changes the topic

        :param topic: The new topic
        :param channel: The channel where the topic changed
        :param who: Nick of the topic changer
        """
        if channel == self.active_window:
            self.hb.set_subtitle('{0}: {1}'.format(channel, topic))

        self.append_status('{2} has changed the topic for {0} to: {1}'.format(channel, topic, who), channel, "bold")


    def append_status(self, status_message, channel, tag_name ="yellow"):
        buffer = self.channel_buffers[channel]

        timestamp = datetime.datetime.now().strftime(self.ui_prefs['time_fmt'])
        buffer.insert(buffer.get_end_iter(), '\n')
        buffer.insert(buffer.get_end_iter(), "{0} ".format(timestamp))
        status_tag = buffer.get_tag_table().lookup(tag_name)
        buffer.insert_with_tags(buffer.get_end_iter(), '*** {0} ***'.format(status_message), status_tag)
        end = buffer.get_insert()
        if channel == self.active_window:
            logger.info('bumping scroll')
            self.channel_view.scroll_to_mark(self.channel_view.get_buffer().get_insert(), 0.0, True, 0, 1 )
        else:
            self.pulse(channel)


    def append_channel_message(self, message, who, channel):
        """ Controller callback to update the chat view with a new message

        :param message: The message
        :param who: Who sent it
        :param channel: What channel
        """
        highlight = self.ic.nick in message and who != self.ic.nick and channel.startswith('#')
        buffer = self.channel_buffers[channel]
        timestamp = datetime.datetime.now().strftime(self.ui_prefs['time_fmt'])

        buffer.insert(buffer.get_end_iter(), '\n')
        if highlight:
            buffer.insert_with_tags(buffer.get_end_iter(), '{0} <'.format(timestamp),
                                    buffer.get_tag_table().lookup('yellow'))
        else:
            buffer.insert(buffer.get_end_iter(), '{0} <'.format(timestamp))

        buffer.insert_with_tags(buffer.get_end_iter(), '{0}'.format(who), buffer.get_tag_table().lookup(who))

        if highlight:
            buffer.insert_with_tags(buffer.get_end_iter(), '> {0}'.format(message),
                                    buffer.get_tag_table().lookup('yellow'))
        else:
            buffer.insert(buffer.get_end_iter(), '> {0}'.format(message))

        if channel == self.active_window:
            end = buffer.get_insert()
            logger.info("Bumping scroll")
            self.channel_view.scroll_to_mark(end, 0.0, True, 0, 1)

        else:
            self.pulse(channel)

    def received_privmsg(self, from_nick, message):
        if from_nick not in self.nick_buttons.keys():
            self.__new_privmsg_window(from_nick, message)
        else:
            self.append_channel_message(message, from_nick, from_nick)



    def user_joining_channel(self, user, channel):
        """ Controller callback when a user joins a channel we are on

        :param user: The user that joined
        :param channel: The channel they joined
        """
        nick, username = user.split('!')
        self.append_status("{0} ({1}) stumbles into {2}".format(nick, username, channel), channel, 'join')
        buffer = self.channel_buffers[channel]
        if not buffer.get_tag_table().lookup(nick):
            buffer.create_tag(nick, foreground_rgba=self.__get_nick_colour(nick))

    def user_leaving_channel(self, user, channel):
        """ Controller callback for a user leaving a channel

        :param user: The user that left
        :param channel: The channel they left
        """
        nick, username = user.split('!')
        self.append_status("{0} ({1}) parts ways with {2}".format(nick, username, channel), channel, 'quit')


    def nicks_in_channel(self, nicks, channel):
        buffer = self.channel_buffers[channel]
        for nick in nicks:
            logger.info('creating tag for {0} in {1}'.format(nick, channel))
            buffer.create_tag(nick, foreground_rgba=self.__get_nick_colour(nick))



    def leaving_channel(self, channel):
        self.append_status("You have left {0}".format(channel), channel)
        chan_btns = self.channel_button_box.get_children()
        if chan_btns:
            chan_btns[0].clicked()


    def user_quitting(self, user, quit_message):
        quit_msg = '{0} has QUIT! ({1})'.format(user, quit_message)
        nick = get_nick(user)
        for channel in self.ic.nicks.keys():
            if nick in self.ic.nicks[channel]:
                self.append_status(quit_msg, channel, 'quit')

        if nick in self.nick_buttons.keys():
            self.append_status(quit_msg, nick)

    def pulse(self, channel):
        self.pthread = threading.Thread(target=self.__pulse, args=[channel])
        self.pthread.start()


def get_palette_colour(index):
    # TODO: Desaturate
    i = index % 256
    r = (128.0 + 128 * sin(3.1415 * i / 32.0)) / 256.0
    g = (128.0 + 128 * sin(3.1415 * i / 64.0)) / 256.0
    b = (128.0 + 128 * sin(3.1415 * i / 128.0)) / 256.0
    a = 1

    logger.debug('Generating colour R:{0}, G:{1}, B:{2}, A:{3}'.format(r, g, b, a))
    col = Gdk.RGBA(r, g, b, a)
    return col


def get_resource_path(rel_path):
    return os.path.abspath(os.path.join(os.path.dirname(__file__), rel_path))

def get_nick(irc_username):
    return irc_username.split('!')[0]

def truncate(sentence, length):
    truncs = []
    if len(sentence) > length:
        prev_break = 0
        while prev_break >= 0 and prev_break < len(sentence):
            next_break = sentence.find(' ', prev_break + length)
            truncs.append(sentence[prev_break:next_break].strip())
            prev_break = next_break
    else:
        truncs = [sentence]
    return truncs

if __name__ == "__main__":
    # Check for config...
    config_dir = os.path.join(os.path.expanduser('~'), CONFIG_DIR)
    logger.info("Checking for existing config in {0} ...".format(config_dir))
    if os.path.exists(config_dir):
        logger.info("Found config.")
    else:
        logger.info("No config found, assuming first run and creating {0} ...".format(config_dir))
        try:
            os.mkdir(config_dir)
        except OSError as e:
            logger.error("Could not create config dir at {0}: {1}".format(config_dir, e.message))
            logger.error("Quitting!")
            sys.exit(-1)

    # Load or create config
    logging.basicConfig(filename=os.path.join(config_dir, 'debug.log'), level=logging.INFO)

    # Launch window!
    irc_view = IrcView(config_dir)
    Gtk.main()
